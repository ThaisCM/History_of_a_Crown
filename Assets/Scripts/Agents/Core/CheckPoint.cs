﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SavePoint
{
    [Scene] public string SceneName;
    public float[] CheckPointPosition;

    public SavePoint(string sceneName, Vector3 position)
    {
        SceneName = sceneName;
        CheckPointPosition = new[] {position.x, position.y, position.z};
    }
}

public class  CheckPoint : MonoBehaviour
{
    public ParticleSystem Fire;
    public ParticleSystem Smoke;
    public AudioClip FireSFX;

    private SavePoint m_SavePoint;

    private void Start()
    {
        m_SavePoint = new SavePoint(gameObject.scene.name, transform.position);
    }

    private void SaveMatch()
    {
        MatchManager.Instance.LastCheckPoint = m_SavePoint;
        
        MatchManager.Instance.SaveGameState();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Smoke.Stop();
            Fire.Play();
            SoundManager.Instance.PlaySound(FireSFX, transform.position);
            MatchManager.Instance.Character.Restore();
            SaveMatch();
        }
    }

    private void Update()
    {
        if (MatchManager.Instance.LastCheckPoint != m_SavePoint)
        {
            Fire.Stop();
            Smoke.Play();
        }
    }
}
