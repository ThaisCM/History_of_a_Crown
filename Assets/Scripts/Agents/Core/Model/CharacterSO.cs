﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "G1/Agent/Character")]
public class CharacterSO : AgentSO
{
    public StatsProgression StatsProgression;

    public string GamePrefab;

    public Sprite Icon;

    public Sprite Portrait;
}

[Serializable]
public class StatsProgression
{
    public int XP_Increment = 2;
    public int Attack_Increment = 2;
    public int Health_Increment = 2;
    public int Defense_Increment = 2;
    public int Stamina_Increment = 2;

    public float XP_Adjustment_Value = 5;
    public float Health_Adjustment_Value = 0.6f;
    public float Attack_Adjustment_Value = 0.6f;
    public float Defense_Adjustment_Value = 0.6f;
    public float Stamina_Adjustment_Value = 0.6f;
}
