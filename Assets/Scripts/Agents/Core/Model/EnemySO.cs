﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "G1/Agent/Enemy")]
public class EnemySO : AgentSO
{
    public int XPOnDeath;
    public int MoneyAmount;
}
