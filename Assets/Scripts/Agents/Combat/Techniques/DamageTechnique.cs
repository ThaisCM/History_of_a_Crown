﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DamageValues
{
    public int Damage;
    public bool IsCritical;

    public DamageValues(int damage, bool critical)
    {
        Damage = damage;
        IsCritical = critical;
    }
}

[CreateAssetMenu(fileName = "DamageTechnique", menuName = "G1/Agent/Combat/Damage Technique")]
public class DamageTechnique : CombatTechnique
{
    [Header("Damage")]
    public Damage Damage;
    public bool SingleHit = true;

    public virtual void OnAgentDamaged(Agent damager, Agent damaged, int damage)
    {
        if (damager is Character)
        {
            damager.GetComponent<CharacterCombatSystem>().OnAgentDamage(damaged, damage, this);
        }
    }

    public virtual void OnAgentKill(Agent damager, Agent damaged, int damage)
    {
        if (damager is Character)
        {
            damager.GetComponent<CharacterCombatSystem>().OnAgentKill(damaged, damage, this);
        }
    }
}

[Serializable]
public struct Damage
{
    [TagSelector]
    public string[] Target;
    public int Value;

    public Damage(string[] target, int value)
    {
        Target = target;
        Value = value;
    }
}
