﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombatSystem : AgentCombatSystem
{
    public void PerformAttack(string parameter, bool isBool = false)
    {
        TriggerTechnique(parameter, isBool);
    }
}
