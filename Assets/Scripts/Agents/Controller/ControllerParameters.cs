﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ControllerParameters
{
    [Header("Gravity")]
    public float Gravity = -30f;
    
    public float FallMultiplier = 1f;
    
    public float AscentMultiplier = 1f;
    
    [Header("Speed")]
    public float SpeedAccelerationOnGround = 20f;

    public float SpeedAccelerationOnAir = 5f;

    public float SpeedFactor = 1f;
    
    [Header("Slopes")]
    [Range(0, 90)]
    public float MaximumSlopeAngle = 30f;
}
