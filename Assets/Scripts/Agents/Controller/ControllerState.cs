﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerState
{
    public bool IsCollidingRight { get; set; }
    public bool IsCollidingLeft { get; set; }
    public bool IsCollidingAbove { get; set; }
    public bool IsCollidingBelow { get; set; }
    public bool HasCollisions { get { return IsCollidingRight || IsCollidingLeft || IsCollidingAbove || IsCollidingBelow; } }
    public bool IsGrounded { get { return IsCollidingBelow; } }
    
    public float SlopeDistance { get; set; }
    
    public float SlopeAngle { get; set; }
    
    public Vector3 SlopeNormal { get; set; }
    
    public bool ClimbingSlope { get; set; }
    
    public bool DescendingSlope { get; set; }
    
    public bool SlidingDownMaxSlope { get; set; }
    
    public void Reset()
    {
        IsCollidingLeft = IsCollidingRight = IsCollidingAbove = IsCollidingBelow = false;

        ClimbingSlope = DescendingSlope = SlidingDownMaxSlope = false;

        SlopeNormal = Vector3.zero;
        SlopeDistance = SlopeAngle = 0;
    }
}

