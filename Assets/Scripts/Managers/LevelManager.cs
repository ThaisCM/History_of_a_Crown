﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static LevelManager m_CurrentLevelManager;
    
    public Transform DebugSpawn;

    public AudioClip BackgroundClip;

    public Zone LevelZone;
    
    public static LevelManager CurrentLevelManager
    {
        get
        {
            if (m_CurrentLevelManager == null)
            {
                return GetComponentInActiveScene<LevelManager>();
            }
            else
            {
                return m_CurrentLevelManager;
            }
        }
    }
    
    public static LevelManager LevelManagerInActiveScene
    {
        get
        {
            return GetComponentInActiveScene<LevelManager>();
        }
    }

    public static T GetComponentInActiveScene<T>()
    {
        GameObject[] gameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (var obj in gameObjects)
        {
            T find = obj.GetComponent<T>();
            if (find != null)
            {
                return find;
            }
        }

        return default;
    }

    private void Awake()
    {
        m_CurrentLevelManager = this;
    }

    private void Start()
    {
        SoundManager.Instance.PlayBackgroundMusic(BackgroundClip);
        gameObject.name = gameObject.scene.name;
    }
}
