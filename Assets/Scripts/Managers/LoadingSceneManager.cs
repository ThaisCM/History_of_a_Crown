﻿using System.Collections;
using System.Collections.Generic;
using PixelCrushers.SceneStreamer;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct TransitionEvent
{
    public enum Type { Start, End, Stop }

    public Type TransitionType;

    public TransitionEvent(Type type)
    {
        TransitionType = type;
    }
}

public class LoadingSceneManager : PersistentSingleton<LoadingSceneManager>
{
    public float DeathFadeInDuration = 0.2f;
    
    public float DeathFadeOutDuration = 0.2f;

    public bool Transitioning { get; private set; }
    
    public void LoadNewScene(string sceneName)
    {
        if (Transitioning)
        {
            return;
        }

        Transitioning = true;

        SceneManager.LoadScene(sceneName);

        Transitioning = false;
    }
    
    public void LoadNewGameScene(string sceneName)
    {
        if (Transitioning)
        {
            return;
        }

        StartCoroutine(LoadNewGameSceneCo(sceneName));
    }
    
    public void LoadSavedScene(SerializedGame serializedGame)
    {
        if (Transitioning)
        {
            return;
        }

        StartCoroutine(LoadSavedSceneCo(serializedGame));
    }
    
    private IEnumerator LoadNewGameSceneCo(string sceneName)
    {
        yield return LoadSceneWithInputLoadingScreen(sceneName);
    }
    
    private IEnumerator LoadSavedSceneCo(SerializedGame serializedGame)
    {
        yield return LoadSceneWithInputLoadingScreen(serializedGame);
    }
    
    private IEnumerator LoadSceneWithInputLoadingScreen(string sceneName)
    {
        Transitioning = true;
        
        yield return SceneManager.LoadSceneAsync("UILoading", LoadSceneMode.Single);
        GUILoading.Instance.Init();
        
        AsyncOperation newScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        newScene.allowSceneActivation = false;

        foreach (var scene in GameManager.Instance.AdditionalGameScenes)
        {
            SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }

        while (!newScene.isDone)
        {
            if (newScene.progress >= 0.9f && !newScene.allowSceneActivation)
            {
                newScene.allowSceneActivation = true;
            }

            yield return null;
        }

        GameManager.Instance.Pause();

        yield return new WaitUntil(() => MatchManager.Instance.MatchLoaded);
        
        GUILoading.Instance.LoadingReady();
        
        while (true)
        {
            if (ReInput.controllers.GetAnyButtonDown())
            {
                GUILoading.Instance.Submit();
                break;
            }

            yield return null;
        }
        
        GameManager.Instance.UnPause();

        Scene sce = SceneManager.GetSceneByName(sceneName);
        SceneManager.SetActiveScene(sce);

        yield return SceneManager.UnloadSceneAsync("UILoading");
        Transitioning = false;
    }
    
    private IEnumerator LoadSceneWithInputLoadingScreen(SerializedGame serializedGame)
    {
        Transitioning = true;
        
        yield return SceneManager.LoadSceneAsync("UILoading", LoadSceneMode.Single);
        GUILoading.Instance.Init();
        
        // AsyncOperation newScene = SceneManager.LoadSceneAsync(serializedGame.SceneName, LoadSceneMode.Additive);
        // newScene.allowSceneActivation = false;
        SceneStreamer.SetCurrentScene(serializedGame.SceneName);

        foreach (var scene in GameManager.Instance.AdditionalGameScenes)
        {
            SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }

        // while (!newScene.isDone)
        // {
        //     if (newScene.progress >= 0.9f && !newScene.allowSceneActivation)
        //     {
        //         newScene.allowSceneActivation = true;
        //     }
        //
        //     yield return null;
        // }
        
        yield return new WaitUntil(() => SceneStreamer.IsSceneLoaded(serializedGame.SceneName));

        MatchManager.Instance.Deserialize(serializedGame);

        GameManager.Instance.Pause();

        yield return new WaitUntil(() => MatchManager.Instance.MatchLoaded);
        
        GUILoading.Instance.LoadingReady();
        
        while (true)
        {
            if (ReInput.controllers.GetAnyButtonDown())
            {
                GUILoading.Instance.Submit();
                break;
            }

            yield return null;
        }
        
        GameManager.Instance.UnPause();

        Scene sce = SceneManager.GetSceneByName(serializedGame.SceneName);
        SceneManager.SetActiveScene(sce);
        
        yield return SceneManager.UnloadSceneAsync("UILoading");

        UIHUD.Instance.UpdateCoins(MatchManager.Instance.CurrentCoins);
        
        Transitioning = false;
    }
    
    public IEnumerator Transition(SavePoint checkpoint)
        {
            if (Transitioning)
            {
                yield break;
            }

            Transitioning = true;
            
            EventManager.TriggerEvent(new TransitionEvent(TransitionEvent.Type.Start));
            
            yield return new WaitForSeconds(DeathFadeInDuration);
            
            EventManager.TriggerEvent(new TransitionEvent(TransitionEvent.Type.Stop));

            SceneStreamer.SetCurrentScene(checkpoint.SceneName);
           
            yield return new WaitUntil(() => SceneStreamer.IsSceneLoaded(checkpoint.SceneName));

            SceneManager.SetActiveScene(SceneManager.GetSceneByName(checkpoint.SceneName));
            
            // Scene newScene = SceneManager.GetSceneByName(checkpoint.SceneName);
            //
            // if (SceneManager.GetActiveScene() == newScene)
            // {
            //     //yield return SceneManager.UnloadSceneAsync(newScene);
            //     //yield return SceneManager.LoadSceneAsync(checkpoint.SceneName, LoadSceneMode.Additive);
            // }
            // else
            // {
            //     if (!newScene.isLoaded)
            //     {
            //         //yield return SceneManager.LoadSceneAsync(checkpoint.SceneName, LoadSceneMode.Additive);
            //     }
            //
            //     //ChangeActiveScene(SceneManager.GetSceneByName(checkpoint.SceneName));
            // }
            
            yield return new WaitForSeconds(DeathFadeOutDuration);
            
            if (!MatchManager.Instance.Character.IsDead) MatchManager.Instance.Character.transform.position = new Vector3(checkpoint.CheckPointPosition[0], checkpoint.CheckPointPosition[1], checkpoint.CheckPointPosition[2]);
                
            EventManager.TriggerEvent(new TransitionEvent(TransitionEvent.Type.End));

            Transitioning = false;
        }
    
    private void ChangeActiveScene(Scene newScene)
    {
        Scene currentScene = SceneManager.GetActiveScene();
        Hide(currentScene);
        SceneManager.UnloadSceneAsync(currentScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        SceneManager.SetActiveScene(newScene);

        Resources.UnloadUnusedAssets();
    }
    
    public void Hide(Scene scene)
    {
        foreach (var go in scene.GetRootGameObjects())
        {
            go.SetActive(false);
        }
    }
}
