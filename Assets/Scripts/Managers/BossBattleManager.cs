﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattleManager : MonoBehaviour
{
    public GameObject paredBoss;

    public AudioClip BossBattleSFX;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            paredBoss.SetActive(true);
            SoundManager.Instance.StopBackgroundMusic();
            SoundManager.Instance.PlayBackgroundMusic(BossBattleSFX);
        }
    }
}
