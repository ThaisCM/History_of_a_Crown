using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class AtDistance : Conditional
{

    private GameObject _Player;

    public float distanciaAtaque = 1f;

    public override void OnAwake()
    {
        _Player = GameObject.FindGameObjectWithTag("Player");
    }

    public override TaskStatus OnUpdate()
	{
        if (_Player == null)
        {
            _Player = GameObject.FindGameObjectWithTag("Player");
        }
        if (Vector3.SqrMagnitude(transform.position - _Player.transform.position) < distanciaAtaque) {
            return TaskStatus.Success;
        }
        return TaskStatus.Failure;
	}
}