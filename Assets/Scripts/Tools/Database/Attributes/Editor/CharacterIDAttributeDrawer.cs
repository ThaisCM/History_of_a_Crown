﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(CharacterIDAttribute))]
public class CharacterIDAttributeDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 50;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var list = CharacterDB.Instance.GetIds();
        List<string> stringList = list.ConvertAll<string>(x => x.ToString());

        EditorGUI.LabelField(position, "Character ID");
        property.intValue = EditorGUI.IntPopup(new Rect(position.x, position.y + 20, position.width, position.height), property.intValue, stringList.ToArray(), list.ToArray());
    }
}
