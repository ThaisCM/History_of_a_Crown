﻿using System.Collections;
using System.Collections.Generic;
using PetoonsStudio.Tools;
using UnityEditor;
using UnityEngine;

public static class QuestDBEditorWindow 
{
    private static Vector2 m_Scoll;
        private static Quest m_HoveredQuest;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("QuestDB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scoll = EditorGUILayout.BeginScrollView(m_Scoll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredQuest = null;
            }

            if (QuestDB.Instance.RowCount > 0)
            {
                for (int i = 0; i < QuestDB.Instance.Count; i++)
                {
                    DrawQuest(i, QuestDB.Instance.GetAtIndex(i));
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(QuestDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredQuest != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveQuest(m_HoveredQuest));
                        }
                        menu.AddItem(new GUIContent("Add"), false, () => AddNewQuest());
                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(75));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(200));
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(300));
            GUILayout.Label("Description");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(150));
            GUILayout.Label("Conditions");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(100));
            GUILayout.Label("Required Level");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(200));
            GUILayout.Label("Rewards");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box");
            GUILayout.Label("Goals");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawQuest(int i, Quest quest)
        {
            GUILayout.BeginHorizontal("box", GUILayout.Height(75));

            GUILayout.BeginHorizontal("box", GUILayout.Height(75), GUILayout.Width(75), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.Label(quest.Id.ToString(), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false), GUILayout.MaxWidth(67));
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical("box", GUILayout.Height(75), GUILayout.Width(200), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUIUtility.labelWidth = 50;
            quest.Name = EditorGUILayout.TextField("KEY", quest.Name, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false));
            EditorGUILayout.LabelField(StaticLocalizationManager.Translation("QUEST", quest.Name), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.EndVertical();

            GUILayout.BeginVertical("box", GUILayout.Height(75), GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUIUtility.labelWidth = 50;
            EditorStyles.label.wordWrap = true;
            quest.QuestDescriptionTextKey = EditorGUILayout.TextField("KEY", quest.QuestDescriptionTextKey, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false));
            EditorGUILayout.LabelField(StaticLocalizationManager.Translation("QUEST", quest.QuestDescriptionTextKey), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.EndVertical();

            GUILayout.BeginHorizontal("box", GUILayout.Height(75), GUILayout.Width(150), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUIUtility.labelWidth = 50;
            GUILayout.BeginVertical();
            if (quest.QuestConditions.previousQuest != null && quest.QuestConditions.previousQuest.Count > 0)
            {
                Dictionary<Quest, int> conditions = new Dictionary<Quest, int>();
                foreach (var q in quest.QuestConditions.previousQuest)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Quest ID", q.ToString(), GUILayout.Width(60));
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        conditions.Add(quest, q);
                    }
                    GUILayout.EndHorizontal();
                }

                foreach (var key in conditions.Keys)
                {
                    key.QuestConditions.previousQuest.Remove(conditions[key]);
                }
            }
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("+", GUILayout.Width(25)))
            {
                //QuestConditionsEditorWindow window = (QuestConditionsEditorWindow)EditorWindow.GetWindow(typeof(QuestConditionsEditorWindow));
                //window.Open(quest);
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Height(75), GUILayout.Width(100), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            quest.QuestConditions.RequiredLevel = EditorGUILayout.IntField(quest.QuestConditions.RequiredLevel, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false), GUILayout.MaxWidth(92));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Height(75), GUILayout.Width(200), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.BeginVertical();
            EditorGUIUtility.labelWidth = 50;
            quest.QuestReward.MoneyReward = EditorGUILayout.IntField("Money", quest.QuestReward.MoneyReward);
            quest.QuestReward.ExperienceReward = EditorGUILayout.IntField("XP", quest.QuestReward.MoneyReward);
            if (quest.QuestReward.ItemRewards != null && quest.QuestReward.ItemRewards.Count > 0)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Items", GUILayout.Width(50));
                GUILayout.BeginVertical();
                foreach (var r in quest.QuestReward.ItemRewards)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(r.Item.ToString(), GUILayout.MaxWidth(87));
                    EditorGUILayout.LabelField(r.ItemNumber.ToString(), GUILayout.MaxWidth(40));
                    GUILayout.EndHorizontal();
                }
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            DrawQuestGoals(quest);

            GUILayout.EndHorizontal();

            Event e = Event.current;
            Vector2 mousePos = Event.current.mousePosition;


            if (e.type != EventType.Layout)
            {
                Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
                if (windowRect.Contains(mousePos)) m_HoveredQuest = quest;
            }
        }

        private static void DrawQuestGoals(Quest quest)
        {
            GUILayout.BeginHorizontal("box", GUILayout.Height(75), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.BeginVertical();

            if (quest.EnemyAmount != null && quest.EnemyAmount.Count > 0)
            {
                List<KillGoal> list = new List<KillGoal>();
                foreach (var goal in quest.EnemyAmount)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Kill " + goal.ID + " Amount " + goal.RequiredAmount.ToString());
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        list.Add(goal);
                    }
                    GUILayout.EndHorizontal();
                }
                list.ForEach(x => quest.EnemyAmount.Remove(x));
            }
            
            if (quest.ActivateZoneID != null && quest.ActivateZoneID.Count > 0)
            {
                List<ActivateZoneGoal> list = new List<ActivateZoneGoal>();
                foreach (var goal in quest.ActivateZoneID)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Area " + goal.ID);
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        list.Add(goal);
                    }
                    GUILayout.EndHorizontal();
                }
                list.ForEach(x => quest.ActivateZoneID.Remove(x));
            }

            if (quest.ActivateNpcID != null && quest.ActivateNpcID.Count > 0)
            {
                List<ActivateNPCGoal> list = new List<ActivateNPCGoal>();
                foreach (var goal in quest.ActivateNpcID)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("NPC " + goal.ID);
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        list.Add(goal);
                    }
                    GUILayout.EndHorizontal();
                }
                list.ForEach(x => quest.ActivateNpcID.Remove(x));
            }
            
            if (quest.Actions != null && quest.Actions.Count > 0)
            {
                List<TutorialGoal> list = new List<TutorialGoal>();
                foreach (var goal in quest.Actions)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Action " + goal.ID);
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        list.Add(goal);
                    }
                    GUILayout.EndHorizontal();
                }
                list.ForEach(x => quest.Actions.Remove(x));
            }
            

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.Width(25)))
            {
                QuestEditorWindow window = (QuestEditorWindow)EditorWindow.GetWindow(typeof(QuestEditorWindow));
                window.OpenWindow(quest);
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        private static void AddNewQuest()
        {
            EditorUtility.SetDirty(QuestDB.Instance);
            Quest quest = ScriptableObject.CreateInstance<Quest>();
            quest.Id = QuestDB.Instance.GetFirstAvalibleId();
            QuestDB.Instance.Add(quest);
        }

        private static void RemoveQuest(int index)
        {
            EditorUtility.SetDirty(QuestDB.Instance);
            QuestDB.Instance.RemoveAt(index);
        }

        private static void RemoveQuest(Quest quest)
        {
            EditorUtility.SetDirty(QuestDB.Instance);
            QuestDB.Instance.Remove(quest);
        }
}
