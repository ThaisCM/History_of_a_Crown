﻿using System;

[Serializable]
public class SerializedSettings 
{
    public float MusicValue;
    public float SfxValue;

    public string Language;
}
