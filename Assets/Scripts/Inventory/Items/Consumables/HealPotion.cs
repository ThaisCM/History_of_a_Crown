﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealPotion", menuName = "G1/Item/Consumable/Heal Potion")]
public class HealPotion : ConsumableItem, IBuy, ISell
{
    public int BuyValue;
    public int SellValue;

    [Header("Effect")]
    public int HealValue;

    public int BuyPrice => BuyValue;
    public int SellPrice => SellValue;
    
    public override bool Usable(Transform target)
    {
        AgentHealth health = target.GetComponent<AgentHealth>();
        if (health.CurrentHealth >= health.MaximumHealth)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public override bool Use(Transform target)
    {
        AgentHealth health = target.GetComponent<AgentHealth>();
        health.HealDamage(HealValue);
        return true;
    }

    public override string GetDescription()
    {
        string message = base.GetDescription();

        message = message.Replace("%replace%", HealValue.ToString());

        return message;
    }
}
