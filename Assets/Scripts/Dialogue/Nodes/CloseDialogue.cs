﻿[NodeTint("#4286f4")]
public class CloseDialogue : EventDialogue
{
    public override void Trigger()
    {
        DialogueManager.Instance.CloseDialogue();
    }

    public override void Skip()
    {
        Trigger();
    }
}
