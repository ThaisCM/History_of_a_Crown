﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Experimental.UIElements;
using Button = UnityEngine.UI.Button;

public class ScriptSelection : MonoBehaviour
{
    
    public GameObject ModeloChica;
    public GameObject ModeloChico;
    public GameObject ImagenConfirmacion;
    public TextMeshProUGUI Nombre;
    public Button ChicaButton;
    public Button ChicoButton;

    private Vector3 m_PosicionInicialChica;
    private Vector3 m_PosicionInicialChico;
    private int m_CurrentSelection;
    private Animator m_AnimatorChica;
    private Animator m_AnimatorChico;
    private int m_SelectedCharacter = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_AnimatorChica = ModeloChica.GetComponent<Animator>();
        m_AnimatorChico = ModeloChico.GetComponent<Animator>();
        m_PosicionInicialChica = ModeloChica.transform.position;
        m_PosicionInicialChico = ModeloChico.transform.position;
        Nombre.text = GameManager.Instance.PlayerName + LocalizationManager.Instance["SELECTION", "NAME"];
    }

    public void ClickChico()
    {
        ChicaButton.interactable = false;
        m_SelectedCharacter = 2;
        m_AnimatorChico.SetBool("Selected", true);
        ModeloChico.transform.position = new Vector3(-7f, 0f, -8f);
        ImagenConfirmacion.SetActive(true);
    }

    public void ClickChica()
    {
        ChicoButton.interactable = false;
        m_SelectedCharacter = 1;
        m_AnimatorChica.SetBool("Selected",true);
        ModeloChica.transform.position = new Vector3(-4.76f, 0f, -8f);
        ImagenConfirmacion.SetActive(true);
    }

    public void DejarSeleccionar()
    {
        ChicaButton.interactable = true;
        ChicoButton.interactable = true;
        ModeloChica.transform.position = m_PosicionInicialChica;
        m_AnimatorChica.SetBool("Selected", false);
        ModeloChico.transform.position = m_PosicionInicialChico;
        m_AnimatorChico.SetBool("Selected", false);
    }

    public void Back()
    {
        LoadingSceneManager.Instance.LoadNewScene(GameManager.Instance.ExitMatchScene);
    }

    public void NewGame()
    {
        GameManager.Instance.NewGame(m_SelectedCharacter);
    }
    
    /*void Update()
    {
        Debug.Log(m_CurrentSelection);
        Debug.Log(m_PosicionInicialChica);
            if (Input.GetKey(KeyCode.A) && m_CurrentSelection == m_TotalCharacters)
            {
                ModeloChica.transform.position = m_PosicionInicialChica;
                m_CurrentSelection = 1;
                PanelChico.SetActive(false);
                PanelChica.SetActive(true);
                ModeloChico.transform.position = new Vector3(m_PosicionInicialChico.x, m_PosicionInicialChico.y, m_PosicionInicialChico.z - 2);  
            } else
    }*/
}
