﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIJournalEntrySlot : MonoBehaviour
{
    public TextMeshProUGUI EntryText;
    
    public JournalEntryStatus Entry { get; set; }

    public void Init(JournalEntryStatus entry)
    {
        Entry = entry;
        
        EntryText.text = LocalizationManager.Instance["ENTRY", entry.Entry.NameKey];
    }
}
