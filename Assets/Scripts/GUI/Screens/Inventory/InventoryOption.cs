﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryOption : Selectable, ISubmitHandler, ICancelHandler
{
    public Image ItemIcon;
    public TextMeshProUGUI Quantity;
    
    public ListedItem Item { get; set; }

    public void Init(ListedItem item)
    {
        Item = item;

        ItemIcon.sprite = item.Item.Icon;
        Quantity.text = item.Quantity.ToString();
    }
    
    public override void OnMove(AxisEventData eventData)
    {
        if (eventData.moveDir == MoveDirection.Left)
        {
            GUIInventory.Instance.PreviousCategory();
        }
        else if (eventData.moveDir == MoveDirection.Right)
        {
            GUIInventory.Instance.NextCategory();
        }
        else
        {
            base.OnMove(eventData);
        }
    }

    public void OnCancel(BaseEventData eventData)
    {
        GUIManager.Instance.CloseInventory();
    }
    
    public void OnSubmit(BaseEventData eventData)
    {
        if (Item != null)
        {
            if (Item.Item is UsableItem)
            {
                UsableItem usable = Item.Item as UsableItem;
                if (usable.Usable(GUIInventory.Instance.Opener))
                {
                    usable.Use(GUIInventory.Instance.Opener);

                    if (Item.Item is ConsumableItem)
                    {
                        Inventory.Instance.RemoveItem(usable, 1);
                        EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Item,Item.Item.Id));
                    }

                    GUIInventory.Instance.ReloadContent();
                }
                else
                {
                    GUIInventory.Instance.ShowOverlayMessage(LocalizationManager.Instance["ITEM", "NOT_USABLE"], TextAnchor.MiddleCenter, gameObject);
                }
            }
        }
    }

    public override void OnSelect(BaseEventData eventData)
    {
        if (Item != null)
        {
            var color = GUIInventory.Instance.ItemIcon.color;
            color = new Color(color.r,color.g,color.b, 1f);
            GUIInventory.Instance.ItemIcon.color = color;
            GUIInventory.Instance.ItemIcon.sprite = Item.Item.Icon;
            GUIInventory.Instance.ItemName.text = LocalizationManager.Instance["ITEM", Item.Item.Name];
            GUIInventory.Instance.ItemDescription.text = Item.Item.GetDescription();
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(eventData.pointerEnter);
    }

    public void Refresh()
    {
        ItemIcon.sprite = Item.Item.Icon;
        Quantity.text = Item.Quantity.ToString();
    }
}
