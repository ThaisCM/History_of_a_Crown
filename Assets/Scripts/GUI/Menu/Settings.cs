﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "G1/Options/Settings")]
public class Settings : ScriptableObject
{
    [Header("Audio")]
    [Range(0f, 1f)]
    public float MusicDefaultValue = 1f;

    [Range(0f, 1f)]
    public float SfxDefaultValue = 1f;

    [Header("Language")]
    public Language DefaultLanguage;
}
