﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUISettings : MonoBehaviour
{
    [Header("Sliders")]
    public Slider Music;
    public Slider Sfx;
    
    public TextMeshProUGUI Language;

    private List<string> m_LanguageOptions;
    private int m_LanguageValue;
    private CanvasGroup m_OptionsPanel;

    private void Awake()
    {
        m_OptionsPanel = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        Music.minValue = 0f;
        Music.maxValue = 1f;
        Music.value = SoundManager.Instance.MusicVolume;

        Sfx.minValue = 0f;
        Sfx.maxValue = 1f;
        Sfx.value = SoundManager.Instance.SfxVolume;

        m_LanguageOptions = new List<string>();


        int value = 0;
        for (int i = 0; i < LocalizationManager.Instance.AvailableLang.Length; i++)
        {
            m_LanguageOptions.Add(LocalizationManager.Instance["LANGUAGE", LocalizationManager.Instance.AvailableLang[i].NameKey]);
            if (LocalizationManager.Instance.CurrentLanguage.Locale == LocalizationManager.Instance.AvailableLang[i].Locale)
            {
                value = i;
            }
        }

        m_LanguageValue = value;
        Language.text = m_LanguageOptions[m_LanguageValue];
    }
    
    public void ChangeMusicValue()
    {
        SoundManager.Instance.SetMusicVolume(Music.value);
    }
    
    public void ChangeSfxValue()
    {
        SoundManager.Instance.SetSFXVolume(Sfx.value);
    }
    
    public void DefaultSettings()
    {
        Music.value = GameManager.Instance.DefaultSettings.MusicDefaultValue;
        Sfx.value = GameManager.Instance.DefaultSettings.SfxDefaultValue;
        SetGUILanguageValue(GameManager.Instance.DefaultSettings.DefaultLanguage);
    }
    
    public void ExitSettings()
    {
        LocalizationManager.Instance.LoadLanguage(LocalizationManager.Instance.AvailableLang[m_LanguageValue]);

        GameManager.Instance.SaveSettings();
        
        m_OptionsPanel.alpha = 0;
        m_OptionsPanel.interactable = false;
        m_OptionsPanel.blocksRaycasts = false;
    }
    
    public void NextLanguage()
    {
        m_LanguageValue++;
        if (m_LanguageValue >= m_LanguageOptions.Count)
        {
            m_LanguageValue = 0;
        }

        Language.text = m_LanguageOptions[m_LanguageValue];
    }
    
    public void PreviousLanguage()
    {
        m_LanguageValue--;
        if (m_LanguageValue < 0)
        {
            m_LanguageValue = m_LanguageOptions.Count - 1;
        }

        Language.text = m_LanguageOptions[m_LanguageValue];
    }
    
    public void SetGUILanguageValue(Language language)
    {
        for (int i = 0; m_LanguageOptions.Count > i; ++i)
        {
            if (LocalizationManager.Instance.AvailableLang[i].Locale == language.Locale)
            {
                Language.text = m_LanguageOptions[i];
                m_LanguageValue = i;
            }
        }
    }
}
