﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired.Integration.UnityUI;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIManager : Singleton<GUIManager>, EventListener<InventoryEvent>
{
    [Header("Canvas")] 
    public Canvas GameMessageSystemCanvas;
    public Canvas PauseCanvas;
    public Canvas MapCanvas;
    public Canvas InventoryCanvas;
    public Canvas DialogueCanvas;
    public Canvas DiaryCanvas;
    public Canvas GameCanvas;

    [Header("Screens")]
    public GameObject ShopScreen;
    public GameObject MainMenuShopScreen;
    public GameObject AskQuestScreen;
    public GameObject ShowQuestScreen;
    
    [Header("Messages")]
    public GameObject MessageScreen;
    public GameObject AchievementAnnouncement;

    private GameObject m_TmpShopScreen;
    private GameObject m_TmpShopMenuScreen;
    private GameObject m_TmpMessageScreen;
    private GameObject m_TmpAskQuestScreen;
    private GameObject m_TmpShowQuestScreen;
    private GameObject m_AreaName;
    private GameObject m_AchievementAnnouncement;

    private EventSystem m_GUIEventSystem;

    protected override void Awake()
    {
        base.Awake();
        EventSystem.current = GetComponentInChildren<EventSystem>();
    }

    private void OnEnable()
    {
        this.EventStartListening<InventoryEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<InventoryEvent>();
    }

    public void CursorMode(bool active)
    {
        if (active)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    
    public void OnEvent(InventoryEvent eventType)
    {
        switch (eventType.EventType)
        {
            case InventoryEvent.Type.Add:

                string message = LocalizationManager.Instance["MESSAGE", "ITEM_OBTAINED"];
                message = message.Replace("%replace%", LocalizationManager.Instance["ITEM", eventType.Item.Name]);
                message = message.Replace("%quantity%", eventType.Quantity == 1 ? "" : eventType.Quantity.ToString());

                ShowMessage(eventType.Item.Icon, message);
                break;
        }
    }

    public void OpenMenuShopScreen(NPCShop npc, Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        m_TmpShopMenuScreen = Instantiate(MainMenuShopScreen, transform);
        m_TmpShopMenuScreen.GetComponentInChildren<GUIShopMainMenu>().OpenMenu(opener, npc);
    }

    public void CloseMenuShopScreen()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        GameManager.Instance.UnPause();
        Destroy(m_TmpShopMenuScreen);
    }

    public void OpenShopScreen(Shop shop, Transform opener)
    {
        UIHUD.Instance.EnableHUD(false);
        CursorMode(true);
        
        m_TmpShopMenuScreen.GetComponentInChildren<GUIShopMainMenu>().HideMenuNoAnim();
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        GameManager.Instance.Pause();
        m_TmpShopScreen = Instantiate(ShopScreen, transform);
        m_TmpShopScreen.GetComponent<GUIShop>().OpenShop(shop, opener);
    }
    
    public void CloseShopScreen()
    {
        UIHUD.Instance.EnableHUD(true);
        CursorMode(true);
        
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        m_TmpShopMenuScreen.GetComponentInChildren<GUIShopMainMenu>().ShowMenuNoAnim();
        EventSystem.current.SetSelectedGameObject(m_TmpShopMenuScreen.GetComponentInChildren<GUIShopMainMenu>().FirstGameObject);
        GameManager.Instance.UnPause();
        Destroy(m_TmpShopScreen);
    }
    
    public void ShowMessage(Sprite icon, string message)
    {
        if (m_TmpMessageScreen == null)
        {
            m_TmpMessageScreen = Instantiate(MessageScreen, GameMessageSystemCanvas.transform);
        }

        m_TmpMessageScreen.GetComponent<GameMessageSystem>().AddMessage(icon, message);
    }

    public void OpenAskQuestScreen(NPCQuest npc, Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        m_TmpAskQuestScreen = Instantiate(AskQuestScreen, transform);
        m_TmpAskQuestScreen.GetComponentInChildren<GUIAskQuest>().OpenAskQuest(opener, npc);
    }

    public void CloseAskQuestScreen()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        Destroy(m_TmpAskQuestScreen);
    }

    public void OpenShowQuestScreen(Quest quest, Transform opener, NPCQuest npc)
    {
        UIHUD.Instance.EnableHUD(false);
        CursorMode(true);

        m_TmpAskQuestScreen.GetComponentInChildren<GUIAskQuest>().HideMenuNoAnim();
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        GameManager.Instance.Pause();
        m_TmpShowQuestScreen = Instantiate(ShowQuestScreen, transform);
        m_TmpShowQuestScreen.GetComponent<GUIShowQuest>().OpenShowQuest(quest, opener, npc);
    }

    public void CloseShowQuestScreen()
    {
        UIHUD.Instance.EnableHUD(true);
        CursorMode(true);
        
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        m_TmpAskQuestScreen.GetComponentInChildren<GUIAskQuest>().ShowMenuNoAnim();
        EventSystem.current.SetSelectedGameObject(m_TmpAskQuestScreen.GetComponentInChildren<GUIAskQuest>().FirstGameObject);
        GameManager.Instance.UnPause();
        Destroy(m_TmpShowQuestScreen);
    }

    public void OpenMap(Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        
        GameManager.Instance.Pause();
        
        MapCanvas.gameObject.SetActive(true);

        UIHUD.Instance.EnableHUD(false);
    }
    
    public void CloseMap()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        
        GameManager.Instance.UnPause();

        MapCanvas.gameObject.SetActive(false);

        UIHUD.Instance.EnableHUD(true);
    }
    
    public void OpenPauseMenu(Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        
        GameManager.Instance.Pause();

        PauseCanvas.gameObject.SetActive(true);

        GUIPause.Instance.OpenMenu();
    }
    
    public void ClosePauseMenu()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        
        GameManager.Instance.UnPause();

        GUIPause.Instance.CloseMenu();

        PauseCanvas.gameObject.SetActive(false);
    }

    public void OpenInventory(Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        
        GameManager.Instance.Pause();
        
        InventoryCanvas.gameObject.SetActive(true);

        GUIInventory.Instance.OpenInventory(opener);
    }

    public void CloseInventory()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        
        GameManager.Instance.UnPause();

        GUIInventory.Instance.CloseInventory();
        
        InventoryCanvas.gameObject.SetActive(false);
    }

    public void OpenDialogue(DialogueGraph dialogue, Transform opener, Transform trigger, int timesTalked)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        DialogueManager.Instance.StartDialogue(dialogue, opener, trigger, timesTalked);

    }

    public void CloseDialogue()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        DialogueManager.Instance.CloseDialogue();
    }

    public void OpenDiary(Transform opener)
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(null, opener.GetComponent<PlayerInput>().Player);
        CursorMode(true);
        
        EventManager.TriggerEvent(new BlockCharacter(BlockCharacter.Type.BlockMovement));
        
        DiaryCanvas.gameObject.SetActive(true);
        
        GUIDiary.Instance.OpenDiary();
    }

    public void CloseDiary()
    {
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).DeactivateModule();
        CursorMode(false);
        
        EventManager.TriggerEvent(new BlockCharacter(BlockCharacter.Type.Unblock));

        GUIDiary.Instance.CloseDiary();
        
        DiaryCanvas.gameObject.SetActive(false);
    }

    public void ShowZoneName(Zone zone)
    {
        if (m_AreaName != null)
        {
            m_AreaName.SetActive(false);
        }
        m_AreaName = Instantiate(zone.ZoneTitlePrefab, GameCanvas.transform);
        Destroy(m_AreaName, m_AreaName.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length);
    }

    public void ShowNewAchievement()
    {
        if (m_AchievementAnnouncement != null)
        {
            m_AchievementAnnouncement.SetActive(false);
        }
        m_AchievementAnnouncement = Instantiate(AchievementAnnouncement, GameCanvas.transform);
        Destroy(m_AchievementAnnouncement, m_AchievementAnnouncement.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length);
    }

}
