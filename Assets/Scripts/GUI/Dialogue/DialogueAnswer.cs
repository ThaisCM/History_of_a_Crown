﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueAnswer : MonoBehaviour
{
    private DialogueBox m_DialogueBox;

    private void Awake()
    {
        m_DialogueBox = GetComponentInParent<DialogueBox>();
    }

    public void NextAnswer()
    {
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            if (transform.parent.GetChild(i) == transform)
            {
                m_DialogueBox.AnswerDialogue(i);
                return;
            }
        }
    }
}
